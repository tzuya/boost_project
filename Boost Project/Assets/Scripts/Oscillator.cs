﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent] //!!!!!!!Con [DisallowMultipleComponent] Solo uno script di questo tipo potra essere collegato ad ogni oggetto UNITY!!!!!!!
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 movementVector= new Vector3(-8f,-11f,0f); //Dichiaro un [SerializeField] visibile in unity di tipo vector3 che conterrà il movimento da aggiungere alla posizione di partenza
    [Range (0,1)][SerializeField] float movementFactor; //fattore di movimento (0 posizione iniziale / 1 massimo movimento), con [Range (0,1)] definisco i limiti della varibile
    Vector3 startingPos; //Variabile che salva la posizione iniziale
    [SerializeField] float movementPeriod = 2f; //valore del periodo del movimento

    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position; //Salvo la posizione attuale
    }

    // Update is called once per frame
    void Update()
    {
        if (movementPeriod<=Mathf.Epsilon) { return; } //Evito la divisione per zero in caso movementPeriod venga assegnato a 0, per faro controllo se è piu picclo della risoluzione minima di un float (comando Mathf.Epsilon)
        float cycles = Time.time / movementPeriod; //Calcolo il numero di cicli (tempo di gioco/periodo del movimento), valore che incrementa all'aumentare del tempo di gioco
        const float tau = Mathf.PI * 2; //Dichiaro una costante 2PI Greco
        float rawSinVal = Mathf.Sin(cycles * tau); //Calcolo il valore SIN tra -1/+1
        movementFactor = (rawSinVal / 2 )+ 0.5f; //Prendo il valore di rawSinVal lo divido 2 e aggiungo 0.5 (cosi ottengo un valore con range tra 0 e 1)
        Vector3 offset = movementFactor * movementVector; //Calcolo la nuova posizione in base al fattore di spostamento
        transform.position = startingPos + offset; //Modifico la posizione dell' oggetto assegnano quella di partenza+l'offset
    }
}
