﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    //VARIABILI
    Rigidbody rigidBody; //Dichiaro una variabile di tipo RigidBody di UNITY (con R maiuscola per indicare la classe UNITY) alla mia variabile locale rigidBody (con r minuscola)
    AudioSource audioSource; //Dichiaro una variabile di tipo AudioSource di UNITY (con A maiuscola per indicare la classe UNITY) alla mia variabile locale audioSource (con a minuscola)
    [SerializeField] float rcsEngine = 100f; //Moltiplicatore per velocità di rotazinione*frame della rotazione, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script
    [SerializeField] float rcsRocket = 100f; //Moltiplicatore per velocità di rotazinione*frame della spinta, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script

    [SerializeField] AudioClip mainEngine; // Dichiaro un audio clip che usero per il suono dei motori, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script
    [SerializeField] AudioClip levelComplete; // Dichiaro un audio clip che usero per il suono di livello completo, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script
    [SerializeField] AudioClip impactExplosion; // Dichiaro un audio clip che usero per il suono in caso di game-over, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script

    [SerializeField] ParticleSystem particlesMainEngine1; // Dichiaro un ParticleSystem che usero per l'effetto dei motori, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script
    [SerializeField] ParticleSystem particlesMainEngine2; // Dichiaro un ParticleSystem che usero per l'effetto dei motori, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script
    [SerializeField] ParticleSystem particlesLevelComplete; // Dichiaro un ParticleSystem che usero per l'effetto di livello completo, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script
    [SerializeField] ParticleSystem particlesImpactExplosion; // Dichiaro un ParticleSystem che usero per l'effetto in caso di game-over, i [SerializeField] sono propietà/variabili modificabili in UNITY ma non da altri script


    enum State { Alive,Died,Trascending,DebugMode } //Enum type per salvare lo stato di gioco
    State state = State.Alive;
    
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();//Associo la mia variabile locale al Rigidbody del componet "Rocket Ship" del progetto UNITY
        audioSource = GetComponent<AudioSource>();//Associo la mia variabile locale al AudioSource del componet "Rocket Ship" del progetto UNITY
    }

    // Update is called once per frame
    void Update()
    {
        //Attivazione motori
        if (state == State.Alive || state == State.DebugMode)
        {
            EngineControl(); //Eseguo la funzione di controllo dei motori
            RotateRocket();//Eseguo la routine per verificare il tasto premuto
        }

        //Attivo i tasti di debug solo se è una build di debug
        if (Debug.isDebugBuild)
        {
            DebugKeys();
        }


    }

    //Funzione dei tasti debug
    private void DebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L)) //Se premo L salto al livello successivo
        {
            LoadNextScene();
        }

        if (Input.GetKeyDown(KeyCode.C)) //Se premo C disbilito le collisioni
        {
            state = State.DebugMode;
            print("DEBUG MODE ON");
        }
    }

    //Funzione di riconoscimento delle collisioni del razzo
    private void OnCollisionEnter(Collision collision) // Funzione standard di UNITY il parametro collision di tipo Collision contiene i parametri di collisione
    {

        if (state != State.Alive && state != State.DebugMode) { return; } //Se sono già morto posso uscire dalla routine

        switch (collision.gameObject.tag) //discrimino in base al valore del TAG di UNITY
        {
            case "Friendly": //Se il TAG è di tipo "Friendly" (Vedi UNITY)
                {
                    //DO NOTHING!
                    print("At Home");
                    break;

                }
            case "Finish": //Se il TAG è di tipo "Finish" (Vedi UNITY)
                {
                    state = State.Trascending;
                    print("Well done!!");
                    audioSource.Stop();
                    audioSource.PlayOneShot(levelComplete); //Faccio partire l'effetto del livello completo (Associato in UNITY) usando PLAYONESHOT cosi posso usare piu suoni
                    particlesMainEngine1.Stop(); //Fermo l'effetto Particles dei motori
                    particlesMainEngine2.Stop(); //Fermo l'effetto Particles dei motori
                    particlesLevelComplete.Play(); //Avvio l'effetto Particles del livello completo
                    Invoke("LoadNextScene",1.5f);//Funzione di carico nuovo livello (Vedi funzione sotto) con ritardo di 1s
                    break;
                }
            default:
                {
                    if (state != State.DebugMode)
                    {
                        state = State.Died;
                        print("Dead");
                        audioSource.Stop();
                        audioSource.PlayOneShot(impactExplosion); //Faccio partire l'effetto audio del game over (Associato in UNITY) usando PLAYONESHOT cosi posso usare piu suoni
                        particlesMainEngine1.Stop(); //Fermo l'effetto Particles dei motori
                        particlesMainEngine2.Stop(); //Fermo l'effetto Particles dei motori
                        particlesImpactExplosion.Play(); //Avvio l'effetto Particles del game over
                        Invoke("ReloadInitialLevel", 1.5f); // Funzione che ricarica il livello(Vedi funzione sotto) con ritardo di 1s
                        
                    }
                    break;
                }
        }
    }

    private void ReloadInitialLevel()
    {
        SceneManager.LoadScene(0);// Funzione che ricarica il livello, necessita using UnityEngine.SceneManagement;
    }

    //Funzione di carico nuovo livello
    private void LoadNextScene()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex; //Salvo l'indice del livello attuale
        int nextScene= currentScene+1;//Preparo l'indice della variabile del livello successivo
        if (nextScene==SceneManager.sceneCountInBuildSettings)//se è l'ultimo livello ritorno a zero (livello iniziale)
        {
            nextScene = 0;
        }
        SceneManager.LoadScene(nextScene); // Funzione che carica il livello successivo, necessita using UnityEngine.SceneManagement;
    }

    //Funzione di rotazione del razzo
    private void RotateRocket()
    {
        rigidBody.freezeRotation = true; // Blocco le rotazioni applicate dalla fisca di rigidbody durante la rotazione dell'utente
        float rotationFrame = rcsEngine * Time.deltaTime;//Imprimo una velocità di roatazione (usando Time.deltaTime questa sarà indipendente dal refresh rate del dispositivo)


        //Controllo senso di rotazione
        if (Input.GetKey(KeyCode.A)) //Controllo se è stato premuto "A"
        {
            //Accedo alla propieta transform di UNITY dell'oggetto "Rocket Ship"
            //imprimendo una rotazione tramite Vector3.forward (ASSE Z),  
            //la moltiplico per rotationSpeed (Vedi variabile) per decidere la velocità di rotazione
            transform.Rotate(Vector3.forward * rotationFrame);
        }
        else if (Input.GetKey(KeyCode.D)) //Controllo se è stato premuto "D"
        {

            //Accedo alla propieta transform di UNITY dell'oggetto "Rocket Ship"
            //imprimendo una rotazione tramite Vector3.back (ASSE Z),  
            //la moltiplico per rotationSpeed (Vedi variabile) per decidere la velocità di rotazione
            transform.Rotate(Vector3.back * rotationFrame);
        }

        rigidBody.freezeRotation = false; // Sblocco le rotazioni applicate dalla fisca di rigidbody
    }

    //Funzione di controllo dei motori
    private void EngineControl()
    {
            if (Input.GetKey(KeyCode.Space)) //Controllo se è stato premuto "Space"
            {
                rigidBody.AddRelativeForce(Vector3.up * rcsRocket); //Aggiungo una spinta relatica al rigid body del componente "Rocket Ship" usando Vecotr3 UP (ASSE Y)
                particlesMainEngine1.Play(); //Avvio l'effetto Particles dei motori
                particlesMainEngine2.Play(); //Avvio l'effetto Particles dei motori
            if (!audioSource.isPlaying) //Se l'effetto audio non è già in esecuzione
                    {
                    //audioSource.Play(); //Faccio partire l'effetto audio dei motori (Associato in UNITY)
                    audioSource.PlayOneShot(mainEngine); //Faccio partire l'effetto audio dei motori (Associato in UNITY) usando PLAYONESHOT cosi posso usare piu suoni
                    }              
            }
            else//Se i motori sono spenti (non c'e lo spazio premuto) spengo l'audio
            {
                audioSource.Stop(); //Fermo l'effetto audio dei motori (Associato in UNITY)
                particlesMainEngine1.Stop(); //Fermo l'effetto Particles dei motori
                particlesMainEngine2.Stop(); //Fermo l'effetto Particles dei motori
        }
    }
}
